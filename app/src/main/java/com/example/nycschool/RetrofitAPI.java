package com.example.nycschool;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitAPI {
    // as we are making get request so we are displaying
    // GET as annotation.
    // and inside we are passing last parameter for our url.
    @GET("s3k6-pzi2.json")

    // as we are calling data from array so we are calling
    // it with array list and naming that method as getAllSchools();
    Call<ArrayList<RecyclerData>> getAllSchools();

    @GET("f9bf-2cp4.json")
    Call<ArrayList<SchoolDetailData>> getSchoolDetails();

}
