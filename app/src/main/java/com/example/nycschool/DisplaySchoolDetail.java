package com.example.nycschool;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplaySchoolDetail extends AppCompatActivity {
    private ArrayList<SchoolDetailData> recyclerDataArrayList;
    private TextView textView_school;
    private TextView textView_reading;
    private TextView textView_math;
    private TextView textView_writting;
    static String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_school_detail);
        recyclerDataArrayList = new ArrayList<>();
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        getSchoolDetails();
    }

    private void getSchoolDetails() {
        // on below line we are creating a retrofit
        // builder and passing our base url
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // below line is to create an instance for our retrofit api class.
        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);

        // on below line we are calling a method to get all the School details from API.
        Call<ArrayList<SchoolDetailData>> call = retrofitAPI.getSchoolDetails();

        // on below line we are calling method to enqueue and calling
        // all the data from array list.
        call.enqueue(new Callback<ArrayList<SchoolDetailData>>() {

            @Override
            public void onResponse(Call<ArrayList<SchoolDetailData>> call, Response<ArrayList<SchoolDetailData>> response) {

                recyclerDataArrayList = response.body();
                for(SchoolDetailData data : recyclerDataArrayList){
                    textView_school = findViewById(R.id.textView_school);
                    textView_reading = findViewById(R.id.textView_reading);
                    textView_math = findViewById(R.id.textView_math);
                    textView_writting = findViewById(R.id.textView_writting);

                    if(data.getDbn().trim().equals(message)) {
                        textView_school.setText(data.getSchoolName());
                        textView_reading.setText(data.getSat_critical_reading_avg_score());
                        textView_math.setText(data.getSat_math_avg_score());
                        textView_writting.setText(data.getSat_writing_avg_score());
                        break;
                    }else {
                        textView_school.setText("No Data Found");
                    }
                }

            }

            @Override
            public void onFailure(Call<ArrayList<SchoolDetailData>> call, Throwable t) {
                Toast.makeText(DisplaySchoolDetail.this, "Fail to get data", Toast.LENGTH_SHORT).show();
            }


        });
    }
}