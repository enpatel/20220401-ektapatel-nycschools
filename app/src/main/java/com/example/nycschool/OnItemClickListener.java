package com.example.nycschool;

public interface OnItemClickListener {
    void userItemClick(int pos);
}
