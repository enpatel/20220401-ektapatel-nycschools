package com.example.nycschool;

public class RecyclerData {

    private String school_name;
    private String dbn;

    public String getSchoolName() {
        return school_name;
    }

    public void setSchoolName(String school_name) {
        this.school_name = school_name;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }


    public RecyclerData(String school_name, String dbn) {
        this.school_name = school_name;
        this.dbn = dbn;
    }
}