package com.example.nycschool;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    // creating a variable for our array list and context.
    private ArrayList<RecyclerData> schoolDataArrayList;
    private Context context;

    // creating a constructor class.
    public RecyclerViewAdapter(ArrayList<RecyclerData> recyclerDataArrayList, Context context) {
        this.schoolDataArrayList = recyclerDataArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate Layout
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        return new RecyclerViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        // Set the data to textview from our modal class.
        RecyclerData modal = schoolDataArrayList.get(position);
        holder.schoolNameTV.setText(modal.getSchoolName());

    }

    @Override
    public int getItemCount() {
        // this method returns the size of recyclerview
        return schoolDataArrayList.size();
    }

    // View Holder Class to handle Recycler View.
    public class RecyclerViewHolder extends RecyclerView.ViewHolder{
        // creating variables for our views.
        private TextView schoolNameTV;
        public LinearLayout ll;

        public RecyclerViewHolder(@NonNull View itemView, final Context context) {
            super(itemView);
            // initializing our views with their ids.
            schoolNameTV = itemView.findViewById(R.id.idTVSchoolName);

            ll = (LinearLayout) itemView.findViewById(R.id.ll_layout);

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) context).userItemClick(getAdapterPosition());
                }
            });
        }
    }
}