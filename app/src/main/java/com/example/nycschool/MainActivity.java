package com.example.nycschool;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements OnItemClickListener  {

    public static final String EXTRA_MESSAGE = "com.example.nycschool.MESSAGE";
    // creating a variable for recycler view,
    // array list and adapter class.
    private RecyclerView schoolsRV;
    private ArrayList<RecyclerData> recyclerDataArrayList;
    private RecyclerViewAdapter recyclerViewAdapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initializing our variables.
        schoolsRV = findViewById(R.id.idRVSchools);
        progressBar = findViewById(R.id.idPBLoading);

        // creating new array list.
        recyclerDataArrayList = new ArrayList<>();

        // calling a method to
        // get all the schools.
        getAllSchools();
    }

    private void getAllSchools() {
        // on below line we are creating a retrofit
        // builder and passing our base url
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // below line is to create an instance for our retrofit api class.
        RetrofitAPI retrofitAPI = retrofit.create(RetrofitAPI.class);

        // on below line we are calling a method to get all the schools from API.
        Call<ArrayList<RecyclerData>> call = retrofitAPI.getAllSchools();

        // on below line we are calling method to enqueue and calling
        // all the data from array list.
        call.enqueue(new Callback<ArrayList<RecyclerData>>() {
            @Override
            public void onResponse(Call<ArrayList<RecyclerData>> call, Response<ArrayList<RecyclerData>> response) {
                // inside on response method we are checking
                // if the response is success or not.
                if (response.isSuccessful()) {
                    // on successful hide progressbar
                    progressBar.setVisibility(View.GONE);

                    // below line is to add our data from api to our array list.
                    recyclerDataArrayList = response.body();

                    // below line we are running a loop to add data to our adapter class.
                    for (int i = 0; i < recyclerDataArrayList.size(); i++) {
                        recyclerViewAdapter = new RecyclerViewAdapter(recyclerDataArrayList, MainActivity.this);

                        // below line is to set layout manager for our recycler view.
                        LinearLayoutManager manager = new LinearLayoutManager(MainActivity.this);

                        // setting layout manager for our recycler view.
                        schoolsRV.setLayoutManager(manager);

                        // below line is to set adapter to our recycler view.
                        schoolsRV.setAdapter(recyclerViewAdapter);

                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<RecyclerData>> call, Throwable t) {
                // in the method of on failure we are displaying a
                // toast message for fail to get data.
                Toast.makeText(MainActivity.this, "Fail to get data", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void userItemClick(int pos) {
        //Toast.makeText(MainActivity.this, "Clicked User : " + recyclerDataArrayList.get(pos).getDbn(), Toast.LENGTH_SHORT).show();
        //OnClick we are calling DisplaySchoolDetails activity where we will call api to get school data
        Intent intent = new Intent(this, DisplaySchoolDetail.class);
        String message = recyclerDataArrayList.get(pos).getDbn();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}